<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('welcome');
});

/*
| Version 1 API Endpoints
*/

/*
| api.v1.post.login
|
| Post user credentials to register API token
*/
$app->post('v1/login', ['middleware' => 'throttle', 'uses' => 'V1\AccountController@login']);

$app->group([
	'prefix' => 'v1', 
	'middleware' => ['auth', 'apps', 'nullJson', 'throttle', 'pageview'],
	'namespace' => 'V1'
	], 
	function () use ($app)
	{

		/*
		| api.v1.post.logout
		|
		| Post nothing to log out
		*/
		$app->post('logout', 'AccountController@logout');

		/*
		| api.v1.get.profile
		|
		| Get user profile by user ID
		*/
		$app->get('profile', 'AccountController@profile');

		/*
		| api.v1.patch.profile
		|
		| Update user profile by user ID
		*/
		$app->patch('profile', 'AccountController@patch');

		/*
		| api.v1.get.topic.single
		|
		| Get single topic by topic ID
		*/
		$app->get('topic', 'TopicController@single');

		/*
		| api.v1.get.topic.list
		|
		| Get list of all subscribed topics by user ID
		*/
		$app->get('topics', 'TopicController@all');

		/*
		| api.v1.get.drug.single
		|
		| Get single drug by drug ID
		*/
		$app->get('drug', 'DrugController@single');

		/*
		| api.v1.get.drug.list
		|
		| Get list of drugs (paginated), provide page number to offset 
		| the result, provide term to search by term
		*/
		$app->get('drugs', 'DrugController@paged');

		/*
		| api.v1.get.journal.single
		|
		| Get single published journal and its list of articles by 
		| journal ID
		*/
		$app->get('journal', 'JournalController@single');

		/*
		| api.v1.get.journal.list
		|
		| Get alphabhetical list of published journals (paginated), 
		| provide page number to offset the result, provide term to 
		| search by term
		*/
		$app->get('journals', 'JournalController@paged');

		/*
		| api.v1.get.article.single
		|
		| Get single published article by ID
		*/
		$app->get('article', 'ArticleController@single');

		/*
		| api.v1.get.article.list
		|
		| Get reverse-chronological list of articles and its 
		| corresponding images and authors within a selected taxonomy 
		| term by term ID
		*/
		$app->get('article/{taxonomy}', 'ArticleController@termPaged');

		/*
		| api.v1.get.article.taxonomy_term.single
		|
		| Get reverse-chronological list of articles and its 
		| corresponding images and authors
		*/
		$app->get('articles', 'ArticleController@paged');
	}
);
