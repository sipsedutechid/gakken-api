<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<title>Gakken Indonesia API</title>
	</head>
	<body>
		<p><img src="/logo-api-services@0.25x.png" alt="Gakken Indonesia API Services" /></p>
		<p>
			Gakken Indonesia Application Programming Interface <?php echo env('APP_VERSION', false) ? 'v' . env('APP_VERSION') : '' ?><br />
			Powered by <?php echo $app->version() ?>
		</p>
		<p>For API support please contact: <a href="mailto:support@gakken-idn.co.id">support@gakken-idn.co.id</a></p>
	</body>
</html>