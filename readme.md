![Gakken Indonesia API Services](http://43.231.128.222/logo-api-services@0.25x.png "Gakken Indonesia API Services")

# Gakken Indonesia REST API Endpoints

Versions - stable `1.0`

The endpoints provided in this application are accessible via base URL `https://api.gakken-idn.id`, primarily for mobile application use. Please contact Gakken Indonesia support for ever more details at 0411-8111255 or support@gakken-idn.co.id

For a full list of endpoints and its details, please refer to [this document](https://drive.google.com/open?id=0BwqoZrH3fTDvR1lhMllNbWlGVEE) at Google Drive.