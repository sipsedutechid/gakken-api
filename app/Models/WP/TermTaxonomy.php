<?php

namespace App\Models\WP;

use Illuminate\Database\Eloquent\Model;

class TermTaxonomy extends Model
{

    public $connection = 'wp';
    public $timestamps = false;
    protected $table = 'term_taxonomy';
    protected $primaryKey = 'term_taxonomy_id';

    public function term()
    {
    	return $this->hasOne('\App\Models\WP\Term', 'term_id', 'term_id')->select(['term_id', 'name']);
    }

    public function posts()
    {
        return $this->belongsToMany('\App\Models\WP\Post', 'term_relationships', 'term_taxonomy_id', 'object_id');
    }

    public function scopeCategories($query)
    {
        return $query->where('taxonomy', 'category');
    }

    public function scopeTags($query)
    {
    	return $query->where('taxonomy', 'post_tag');
    }

    public function scopeTaxonomy($query, $taxonomy)
    {
        return $query->where('taxonomy', $taxonomy);
    }

}