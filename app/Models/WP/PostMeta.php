<?php

namespace App\Models\WP;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{

	public $timestamps = false;
	public $primaryKey = 'meta_id';
	protected $table = 'postmeta';

}