<?php

namespace App\Models\WP;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{

    public $connection = 'wp';
    public $timestamps = false;
    protected $primaryKey = 'term_id';

    public function termTaxonomy()
    {
    	return $this->hasOne('\App\Models\WP\TermTaxonomy');
    }

}