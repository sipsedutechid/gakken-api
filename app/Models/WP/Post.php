<?php

namespace App\Models\WP;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $connection = 'wp';
    public $timestamps = false;

    public function featuredImages()
    {
    	return $this->belongsToMany('\App\Models\WP\Post', 'postmeta', 'post_id', 'meta_value')
    		->select(['guid as url', 'post_mime_type as mime_type'])
    		->where('meta_key', '_thumbnail_id');
    }

    public function subheading()
    {
    	return $this->hasOne('\App\Models\WP\PostMeta')
    		->select(['post_id', 'meta_value as content'])
    		->where('meta_key', 'subheading');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\WP\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id')
            ->categories()
            ->select(['term_id'])
            ->with(['term']);
    }

    public function tags()
    {
    	return $this->belongsToMany('App\Models\WP\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id')
    		->tags()
    		->select(['term_id'])
    		->with(['term']);
    }

    public function relatedPosts()
    {
        return $this->belongsToMany('App\Models\WP\Post', 'postmeta', 'post_id', 'meta_value')
            ->select(['ID as id', 'post_title as title', 'post_date_gmt as created_at', 'post_author'])
            ->with(['featuredImages', 'author'])
            ->where('meta_key', 'related_posts');
    }

    public function author()
    {
    	return $this->belongsTo('\App\Models\WP\User', 'post_author')
    		->select(['ID as id', 'display_name']);
    }

    public function getIsOpinionAttribute()
    {
    	return $this->whereHas('categories', function ($query)
    	{
    		$query->term()->where('slug', 'opini');
    	});
    }

    public function scopePublishedPosts($query)
    {
    	return $query->where('post_type', 'post')
    		->where('post_status', 'publish');
    }

}
