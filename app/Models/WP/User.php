<?php

namespace App\Models\WP;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

	public $connection = 'wp';
	public $timestamps = false;

}