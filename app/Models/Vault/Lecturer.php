<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lecturer extends Model
{

	use SoftDeletes;

    protected $connection = 'vault';
    protected $table = 'lectures';

}