<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{

    protected $connection = 'vault';

    public function articles()
    {
    	return $this->hasMany('App\Models\Vault\Article');
    }

    public function getImageUrlAttribute($value)
    {
    	$imagePath = "/journal/files/image/";
    	return env('VAULT_BASE_URL', '') . $imagePath . $value;
    }

}
