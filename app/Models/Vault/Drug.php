<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{

    protected $connection = 'vault';

    public function manufacturer()
    {
    	return $this->belongsTo('App\Models\Vault\DrugManufacturer', 'drug_manufacturer_id')->select(['id', 'name', 'slug', 'logo_image']);
    }

    public function marketer()
    {
    	return $this->belongsTo('App\Models\Vault\DrugManufacturer', 'drug_marketer_id')->select(['id', 'name']);
    }

    public function drugClass()
    {
    	return $this->belongsTo('App\Models\Vault\DrugClass')->select(['id', 'name']);
    }

    public function subclass()
    {
    	return $this->belongsTo('App\Models\Vault\DrugSubclass', 'drug_subclass_id')->select(['id', 'name']);
    }

    public function forms()
    {
    	return $this->belongsToMany('App\Models\Vault\DrugForm', 'drug_drug_forms')
    		->select(['id', 'name']);
    }

    public function dosages()
    {
    	return $this->hasMany('App\Models\Vault\DrugDosage')
    		->select(['id', 'drug_id', 'name', 'description', 'administration', 'drug_form_id'])
    		->with(['drugForm']);
    }

    public function composition()
    {
    	return $this->hasMany('App\Models\Vault\DrugGeneric')
    		->select(['drug_id', 'generic_id', 'quantity', 'unit', 'drug_form_id'])
    		->with(['generic', 'drugForm']);
    }

    public function packagings()
    {
    	return $this->hasMany('App\Models\Vault\DrugPackaging')
    		->select(['name', 'price', 'quantity', 'quantity_2', 'quantity_3', 'drug_id', 'drug_form_id'])
    		->with(['drugForm']);
    }

    public function getAdministrationAttribute($value)
    {
        switch ($value) {
            case 'cc': $string = 'Berikan bersama makanan'; break;
            case 'scc': $string = 'Berikan tanpa makanan'; break;
            case 's/cc': $string = 'Dapat diberikan bersama atau tanpa makanan'; break;
            default:
                $string = "Tidak ditentukan";
        }

        return $string;        
    }

    public function getClassificationAttribute($value)
    {
        switch ($value) {
            case 'o': $string = "Narkotika"; break;
            case 'g': $string = "Obat Keras"; break;
            case 'w': $string = "Bebas Terbatas"; break;
            case 'b': $string = "Bebas"; break;
            default:
                $string = "Tidak ditentukan";
                break;
        }
        return $string;
    }

    public function getPregnancyCategoryAttribute($value)
    {
        return strtoupper($value);
    }

}
