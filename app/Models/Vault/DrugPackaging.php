<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugPackaging extends Model
{

    protected $connection = 'vault';
    protected $table = 'drug_packaging';

    public function drugForm()
    {
    	return $this->belongsTo('App\Models\Vault\DrugForm')->select(['id', 'name']);
    }

}
