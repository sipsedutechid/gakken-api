<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{

    protected $connection = 'vault';

    public function topics()
    {
    	return $this->belongsToMany('\App\Models\Vault\Topic', 'topic_content');
    }

    public function getDataAttribute($value)
    {
    	switch ($this->type) {
    		case 'file':
    			$filepath = '/topics/files/';
    			$value = env('WP_BASE_URL') . $filepath . $value;
    			break;
            case 'link':
                if ($this->url_jw) {
                    $value = $this->jwEncryptSignedUrl($this->url_jw);
                }
                break;
    		default:
    			break;
    	}
    	return $value;
    }

    public function getAppearanceAttribute($value)
    {
        return $value == 'video' ? 'video-mobile' : $value;
    }

    public function scopeExcludeWebVids($query)
    {
    	return $query->where('appearance', '<>', 'video');
    }

    private function jwEncryptSignedUrl($key)
    {
        $path = "manifests/" . $key;
        $path .= '.m3u8';
        $expires = round((time() + 3600) / 300) * 300;
        $secret = env('JWPLAYER_SECRET');
        $signature = md5($path . ':' . $expires . ':' . $secret);
        return 'http://content.jwplatform.com/'.$path.'?sig=' . $signature . '&exp=' . $expires;;
    }

}
