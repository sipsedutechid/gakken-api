<?php

namespace App\Models\Vault;

use App\Scopes\PublishedTopicScope;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{

    protected $connection = 'vault';

    public function contents()
    {
    	return $this->belongsToMany('\App\Models\Vault\Content', 'topic_content')
    		->select(['id', 'label', 'data', 'type', 'url_jw', 'appearance', 'is_free']);
    }

    public function lecturers()
    {
    	return $this->belongsToMany('\App\Models\Vault\Lecturer', 'topic_lecture', 'topic_id', 'lecture_id')
    		->select(['id', 'name', 'front_title', 'back_title']);
    }

    public function getFeaturedImageAttribute($value)
    {
        return strlen($value) ? env('VAULT_BASE_URL', '') . '/topic/files/' . $value : '';
    }

    public function scopePublished($query)
    {
        return $query->where('post_status', true);
    }

}