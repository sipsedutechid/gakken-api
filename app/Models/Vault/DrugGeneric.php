<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugGeneric extends Model
{

    protected $connection = 'vault';
    protected $table = 'drug_generic';

    public function generic()
    {
    	return $this->belongsTo('App\Models\Vault\Generic')->select(['id', 'name']);
    }

    public function drugForm()
    {
    	return $this->belongsTo('App\Models\Vault\DrugForm')->select(['id', 'name']);
    }

}
