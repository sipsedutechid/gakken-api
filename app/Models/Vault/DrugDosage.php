<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugDosage extends Model
{

    protected $connection = 'vault';
    protected $with = ['drugForm'];

    public function drugForm()
    {
    	return $this->belongsTo('App\Models\Vault\DrugForm')->select(['id', 'name']);
    }

}
