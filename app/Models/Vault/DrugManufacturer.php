<?php

namespace App\Models\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugManufacturer extends Model
{

    protected $connection = 'vault';

    public function getLogoImageAttribute($value)
    {
    	return env('VAULT_BASE_URL') . '/drug/manufacturer/logo/' . $this->slug;
    }

}
