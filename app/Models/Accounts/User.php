<?php

namespace App\Models\Accounts;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{

	use Authenticatable, Authorizable;

	protected $connection = 'accounts';
	protected $hidden = [
        'password',
    ];

	public function detail()
	{
		return $this->hasOne('\App\Models\Accounts\UserDetail')
			->select([
				'user_id',
				'address_1',
				'address_2',
				'city',
				'postal_code',
				'phone'
				]);
	}

	public function subscriptions()
	{
		return $this->hasMany('\App\Models\Accounts\UserSubscription');
	}

	public function userTopics()
	{
		return $this->hasMany('\App\Models\Accounts\UserTopic');
	}

	public function activeSubscription()
	{
		return $this->hasOne('\App\Models\Accounts\UserSubscription')
			->active()
			->select(['user_id', 'subscription_id', 'starts_at', 'expired_at']);
	}

}