<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{

	protected $connection = 'accounts';
	protected $with = ['subscription'];

	public function subscription()
	{
		return $this->belongsTo('\App\Models\Accounts\Subscription')
			->select(['id', 'label']);
	}

	public function scopeActive($query)
	{
		return $query->whereDate('starts_at', '<', date('Y-m-d'))
			->whereDate('expired_at', '>', date('Y-m-d'));
	}

}