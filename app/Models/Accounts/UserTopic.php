<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class UserTopic extends Model
{

    protected $connection = 'accounts';
    protected $with = ['topic'];

    public function topic()
    {
    	return $this->belongsTo('\App\Models\Vault\Topic')
            ->published()
    		->select(['id', 'title', 'summary', 'featured_image', 'category', 'release']);
    }

    public function scopeAccessibles($query)
    {
    	return $query->where('is_accessible', true);
    }

    public function scopeBacknumbers($query)
    {
    	return $query->where('is_backnumber', true);
    }

}