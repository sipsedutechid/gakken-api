<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class PageView extends Model
{

	public $timestamps = false;
	protected $connection = 'accounts';
	protected $fillable = ['user_id', 'url'];

}