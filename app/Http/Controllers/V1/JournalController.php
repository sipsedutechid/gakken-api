<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;

class JournalController extends Controller
{

	public function single(\Illuminate\Http\Request $request)
	{
		$this->validate($request, ['id' => 'required|integer']);
		return \App\Models\Vault\Journal::select([
			'id', 
			'issn', 
			'e_issn',
			'title',
			'description',
			'publisher',
			'image_url',
			'ext_url',
			])
			->findOrFail($request->id);
	}

	public function paged(\Illuminate\Http\Request $request)
	{
		$this->validate($request, [
			'page' => 'integer|min:0', 
			'term' => 'string'
			]);

		$journals = \App\Models\Vault\Journal::select(['id', 'title', 'publisher', 'image_url'])
			->orderBy('title');

		if ($request->input('term')) {
			$journals->orWhere(function ($query) use ($request)
			{
				foreach (explode(' ', $request->input('term')) as $term) {
					$query->where('title', 'like', '%' . $term . '%');
				}
			});
			$journals->orWhereHas('articles', function ($query) use ($request)
			{
				foreach (explode(' ', $request->input('term')) as $term) {
					$query->where('author', 'like', '%' . $term . '%');
				}
			});
		}

		return $journals->offset($request->input('page') ? $request->input('page') - 1 : 0)
			->take(15)
			->get();
	}

}