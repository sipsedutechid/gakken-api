<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

	public function single(\Illuminate\Http\Request $request)
	{
		$this->validate($request, ['id' => 'required|integer|min:1']);

		$post = \App\Models\WP\Post::publishedPosts()
			->select([
				'id',
				'post_author',
				'post_title as title',
				'post_date_gmt as created_at',
				'post_content as content',
				])
			->with([
				'featuredImages',
				'subheading',
				'author',
				'categories',
				'tags',
				'relatedPosts'
				])
			->findOrFail($request->input('id'));

		return $post;
	}

	public function paged(\Illuminate\Http\Request $request)
	{
		$this->validate($request, [
			'page' => 'integer|min:1',
			'term' => 'string'
			]);
		$perPage = 15;
		$posts = \App\Models\WP\Post::publishedPosts()
			->select([
				'id',
				'post_author',
				'post_title as title',
				'post_date_gmt as created_at',
				])
			->with(['featuredImages', 'author']);

		if ($request->input('term')) {
			$posts->where(function ($query) use ($request)
			{
				foreach (explode(' ', $request->input('term')) as $term) {
					$query->where('post_title', 'like', '%' . $request->input('term') . '%');
				}
			});
		}
		return $posts
			->orderBy('post_date_gmt', 'desc')
			->offset($perPage * ($request->input('page') ? $request->input('page') - 1 : 0))
			->take($perPage)
			->get();
	}

	public function termPaged($taxonomy, \Illuminate\Http\Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|min:0',
			'page' => 'integer|min:1'
			]);
		$perPage = 15;
		$posts = \App\Models\WP\Term::where('term_id', $request->input('id'))
			->whereHas('termTaxonomy', function ($query) use ($taxonomy)
			{
				$query->where('taxonomy', $taxonomy);
			})
			->firstOrFail()
			->termTaxonomy
			->posts()
			->publishedPosts()
			->select([
				'id',
				'post_author',
				'post_title as title',
				'post_date_gmt as created_at',
				])
			->with(['featuredImages', 'author'])
			->orderBy('post_date_gmt', 'desc')
			->offset($perPage * ($request->input('page') ? $request->input('page') - 1 : 0))
			->take($perPage)
			->get();
		$term = \App\Models\WP\Term::select(['term_id as id', 'name'])->findOrFail($request->input('id'));

		return response()->json([$taxonomy => $term, 'articles' => $posts]);
	}

}