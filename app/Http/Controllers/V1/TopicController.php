<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{

	public function single(\Illuminate\Http\Request $request)
	{
		$this->validate($request, ['id' => 'required|integer|min:1']);
		$topic = \App\Models\Vault\Topic::select([
			'id', 
			'slug', 
			'title', 
			'featured_image', 
			'category', 
			'publish_at', 
			'summary'
			])
			->with(['contents', 'lecturers'])
			->findOrFail($request->input('id'));

		return $topic;
	}

	public function all()
	{
		$user = Auth::user();
		$accessibles = $user
			->userTopics()
			->accessibles()
			->get();
		$backnumbers = $user
			->userTopics()
			->backnumbers()
			->get();

		foreach ($accessibles as $index => $accessible) {
			if ($accessible->topic == null) {
				unset($accessibles[$index]);
			}
		}

		foreach ($backnumbers as $index => $backnumber) {
			if ($backnumber->topic == null) {
				unset($backnumbers[$index]);
			}
		}

		return response()->json([
			'accessible' => $accessibles,
			'backnumber' => $backnumbers
			]);
	}

}