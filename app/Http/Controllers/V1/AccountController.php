<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

	public function login(\Illuminate\Http\Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email',
			'password' => 'required|string',
			]);

		$password = $request->password;
		$user = \App\Models\Accounts\User::where('email', $request->email)->first();

		if ($user)
			if (\Illuminate\Support\Facades\Hash::check($password, $user->password)) {
				// Register API token
				$token = str_random(32);

				\App\APIToken::create([
					'token' => $token, 
					'user_id' => $user->id,
					'user_agent' => $request->header('user-agent'),
					'expires_at' => strtotime('+7 day')
					]);

				return response()->json([
					'status' => 'authenticated',
					'token' => $token
					]);
			}
			
		return abort(403);

	}

	public function logout(\Illuminate\Http\Request $request)
	{
		\App\APIToken::where('token', $request->header('API-TOKEN'))->delete();
		return response()->json([
			'status' => 'success'
			]);
	}

	public function profile(\Illuminate\Http\Request $request)
	{
		return \App\Models\Accounts\User::select([
			'id', 
			'email', 
			'name', 
			'avatar_url',
			'profession',
			'id_no',
			'created_at'
			])
			->with(['detail', 'activeSubscription'])
			->findOrFail(Auth::user()->id);
	}

	public function patch(\Illuminate\Http\Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string'
			]);
		$user = Auth::user();
		$user->name = clean($request->input('name'), ['HTML.Allowed' => '']);
		$user->save();

		return response("OK");
	}

}