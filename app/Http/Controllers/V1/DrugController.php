<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;

class DrugController extends Controller
{
    public function single(\Illuminate\Http\Request $request)
    {
		$this->validate($request, ['id' => 'required|integer']);
		$drug = \App\Models\Vault\Drug::select([ 
            'id', 
            'name', 
            'drug_manufacturer_id', 
            'drug_marketer_id',
            'drug_class_id', 
            'drug_subclass_id',
            'indication',
            'contraindication',
            'precautions',
            'adverse_reactions',
            'interactions',
            'administration',
            'administration_extra',
            'classification',
            'pregnancy_category'
            ])
            ->with([
                'manufacturer', 
                'marketer',
                'drugClass',
                'subclass',
                'forms',
                'dosages',
                'composition',
                'packagings',
                ])
            ->findOrFail($request->id);

        $images = [
            'Narkotika' => env('WP_BASE_URL') . '/wp-content/themes/gakkentheme/images/narkotika.png',
            'Obat Keras' => env('WP_BASE_URL') . '/wp-content/themes/gakkentheme/images/obat-keras.jpg',
            'Bebas Terbatas' => env('WP_BASE_URL') . '/wp-content/themes/gakkentheme/images/obat-bebas-terbatas.png',
            'Bebas' => env('WP_BASE_URL') . '/wp-content/themes/gakkentheme/images/obat-bebas.png',
        ];
        $drug->classification_image_url = $images[$drug->classification];
		return $drug;
    }

    public function paged(\Illuminate\Http\Request $request)
    {
    	$this->validate($request, [
    		'page' => 'integer|min:0', 
    		'term' => 'string'
    		]);
    	$drugs = \App\Models\Vault\Drug::select(['id', 'name', 'drug_manufacturer_id']);
    	if ($request->input('term')) {
    		$terms = explode(' ', $request->input('term'));
			$drugs->orWhere(function ($query) use ($terms)
			{
	    		foreach ($terms as $term) {
    				$query->where('name', 'like', '%' . $term . '%');
	    		}
			})
			->orWhereHas('manufacturer', function ($query) use ($terms)
			{
				foreach ($terms as $term) {
    				$query->where('name', 'like', '%' . $term . '%');
	    		}
			});
    	}

    	return $drugs
    		->with(['manufacturer' => function ($query)
    		{
    			$query->select(['id', 'name']);
    		}])
    		->offset(15 * ($request->input('page') ? $request->input('page') - 1 : 0))
    		->take(15)
            ->orderBy('name')
    		->get();
    }
}
