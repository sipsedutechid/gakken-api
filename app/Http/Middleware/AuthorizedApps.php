<?php

namespace App\Http\Middleware;

use Log;
use Closure;

class AuthorizedApps
{

    public function handle($request, Closure $next)
    {
        $logIp = $request->ip();
        $logUrl = $request->fullUrl();
        $isProduction = env('APP_ENV', '') == 'production';

        // Invalid headers
        if (
            !$request->hasHeader('API-APP') ||
            !$request->hasHeader('API-APP-KEY')
            ) {
            if ($isProduction) Log::alert('ACCESS_DENIED [url]' . $logUrl . ' [ip]' . $logIp . ' [reason]invalid-headers');
            return response('Unauthorized', 403);
        }

        $apps = [
            'mobile-test'       => 'gk-secretYB3hMpEoMfD7lQssO503T11LQY5V3B77',
            'mobile-android'    => 'gk-secretpMNknvaLSIg8ZFiQmuoQ7TH0qDq1fo6f',
            'mobile-ios'        => 'gk-secret5iNlDIc1o31ABouCfuylWeAJxQ46bu2u',
        ];

        // App Name not found in allowed apps list
        if (!isset($apps[$request->header('API-APP')])) {
            if ($isProduction) Log::alert('ACCESS_DENIED [url]' . $logUrl . ' [ip]' . $logIp . ' [reason]invalid-app');
            return response('Unauthorized', 403);
        }

        // App Key does not match
        if ($apps[$request->header('API-APP')] != $request->header('API-APP-KEY')) {
            if ($isProduction) Log::alert('ACCESS_DENIED [url]' . $logUrl . ' [ip]' . $logIp . ' [app]' . $request->header('API-APP') . ' [reason]invalid-app-key');
            return response('Unauthorized', 403);
        }

        // Log successful access
        if ($isProduction) Log::info('ACCESS_GRANTED [url]' . $logUrl . ' [ip]' . $logIp . ' [app]' . $request->header('API-APP'));

        return $next($request);
    }

}
