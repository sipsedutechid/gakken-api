<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Accounts\PageView;

class RecordPageview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user())
            PageView::create([
                'user_id' => $request->user()->id,
                'url' => $request->fullUrl(),
            ]);

        return $next($request);
    }
}
