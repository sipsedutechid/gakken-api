<?php

namespace App\Http\Middleware;

use Closure;

class NullJsonValues
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($request->wantsJson()) {

            $json = json_decode($response->content());
            // Check if response contains valid JSON string
            if (json_last_error() != JSON_ERROR_NONE)
                return $response;

            $json = $this->replaceNullValues($json);

            return json_encode($json);
        }

        return $response;
    }

    /**
     * Loop through JSON object and replace any null value found.
     */
    public function replaceNullValues($json)
    {
        foreach ($json as $key => $value) {
            if ($value == null)
                $json->$key = "";
            else if (is_object($value) || is_array($value))
                $value = $this->replaceNullValues($value);
        }

        return $json;
    }
}
