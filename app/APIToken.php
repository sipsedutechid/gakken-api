<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APIToken extends Model
{

    protected $connection = 'accounts';
    protected $dates = ['expires_at'];
    protected $fillable = ['token', 'user_id', 'user_agent', 'expires_at'];
    protected $table = 'api_tokens';
    public $timestamps = false;
    public $primaryKey = 'token';
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo('App\Models\Accounts\User');
    }

    public function scopeActive($query)
    {
        return $query->where('expires_at', '>', time());
    }

}
