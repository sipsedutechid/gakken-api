<?php 

return [
    'default' => 'wp',

    'connections' => [
        'wp' => [
            'driver'    => env('WP_DB_CONNECTION', 'mysql'),
            'host'      => env('WP_DB_HOST', 'localhost'),
            'port'      => env('WP_DB_PORT', 3306),
            'database'  => env('WP_DB_DATABASE', 'gakken_web'),
            'username'  => env('WP_DB_USERNAME', 'root'),
            'password'  => env('WP_DB_PASSWORD', 'secret'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => env('WP_DB_PREFIX', 'wp_'),
            'strict'    => false,
        ],
        'accounts' => [
            'driver'    => env('ACCOUNTS_DB_CONNECTION', 'mysql'),
            'host'      => env('ACCOUNTS_DB_HOST', 'localhost'),
            'port'      => env('ACCOUNTS_DB_PORT', 3306),
            'database'  => env('ACCOUNTS_DB_DATABASE', 'gakken_accounts'),
            'username'  => env('ACCOUNTS_DB_USERNAME', 'root'),
            'password'  => env('ACCOUNTS_DB_PASSWORD', 'secret'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => env('ACCOUNTS_DB_PREFIX', ''),
            'strict'    => false,
        ],
        'vault' => [
            'driver'    => env('VAULT_DB_CONNECTION', 'mysql'),
            'host'      => env('VAULT_DB_HOST', 'localhost'),
            'port'      => env('VAULT_DB_PORT', 3306),
            'database'  => env('VAULT_DB_DATABASE', 'gakken_vault'),
            'username'  => env('VAULT_DB_USERNAME', 'root'),
            'password'  => env('VAULT_DB_PASSWORD', 'secret'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => env('VAULT_DB_PREFIX', ''),
            'strict'    => false,
        ],
    ],
    

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],
];
